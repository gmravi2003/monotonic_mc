function[in_mat]=HideElements(in_mat,p)
[n1,n2]=size(in_mat);
random_mat=rand(n1,n2);

flag=random_mat>p;

in_mat(flag)=nan;
end