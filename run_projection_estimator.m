
clear all;
% observed matrix is of size n1 by n2 of rank r
n1=30;
n2=20;
r=5;
  
c=2;
p=0.7;


Ztrue=dlmread('lowrank_matrix.txt'); % This is the original low rank matrix

Xtrue=1./(1+exp(-c*Ztrue));
rank_Xtrue=rank(Xtrue,10^-3);
fprintf('Rank of Xtrue=%d\n',rank_Xtrue);

fname=['xobs_c_',num2str(c),'_p_',num2str(p),'.txt'];
fprintf(fname);
fprintf('\n');


if(~exist(fname))
    fprintf('file does not exist....\n');
    Xobs=HideElements(Xtrue,p);
    dlmwrite(fname,Xobs);
else
    % File exists
    fprintf('File exists...\n');
    Xobs=dlmread(['xobs_c_',num2str(c),'_p_',num2str(p),'.txt']);
end
observed_indices=~isnan(Xobs);
tst_size=numel(Xtrue);
fprintf('Tst size=%d\n',tst_size);
Xobs(isnan(Xobs))=0;
Zest=projection_estimator(Xobs,p,r);
err=norm(Zest-Xtrue,'fro')^2/tst_size;
fprintf('Error of our projection only estimator=%f\n',err);
% learn a g function by calling Lipschitz PAV
% X=g(Z)

zknots=Zest(observed_indices);
xknots=Xobs(observed_indices);
xhat_pav = lpav(zknots,xknots);

Xest=Interpolate(Zest,observed_indices,xhat_pav,zknots,'full');
err_proj=norm(Xest-Xtrue,'fro')^2/tst_size;
fprintf('Error of our projection+LPAV estimator=%f\n',err_proj);
    