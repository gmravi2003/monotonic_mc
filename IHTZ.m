% Iterative hard thresholding routine for lifted
% matrix completion
function[Zt]=IHTZ(Z0,xknots,zknots,xhat_pav,...
    observed_indices,step_size,num_grad_steps,proj_rank,svd_method)

% Perform k gradient steps, each time projection onto 
% the cone of low-rank matrices of rank proj_rank


Zt=Z0;
zt=Z0(observed_indices);
for t=1:num_grad_steps
    if(t>1)
        res=Interpolate(Zt,observed_indices,...
            xhat_pav,zknots,'limited');
    else
        % for the first step res is known
        res=nan(size(Zt));
        res(observed_indices)=xhat_pav;
    end
    xhat=res(observed_indices);
    %if(sum(isnan(xhat(:)))>0)
     %fprintf('There are nans in xhat here..\n');
    %end
    zt=zt-step_size*(xhat-xknots);
    % put it back into matrix form
    Zt(observed_indices)=zt;
    
    Zt=ProjectCone(Zt,proj_rank,svd_method);
    zt=Zt(observed_indices);
end
end