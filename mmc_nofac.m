%%%%% This script takes in the following params %%%%%%%%%%%%%%
% Xobs, proj_dim, observed_indices, omega_size,val_size, val_flag
%%%%%%%%%%%%%% CALCULATIONS BEGIN %%%%%%%%%%%%%
function[Xest,rmse]=mmc_nofac(Xtrn,Xval,step_size,proj_dim,...
                val_indices,maxiter,num_grad_steps,maxrank,incr_rank,...
                incr_rank_flag,decr_step_flag,svd_method,lpav_method,...
                F_mat,rho,tol_mmc)
Zest=Xtrn;
observed_indices=~isnan(Xtrn);
Zest(~observed_indices)=0;
omega_size=sum(observed_indices(:));

[m,n]=size(Xtrn);
size_X=m*n;
Zest=Zest*size_X/omega_size;
zknots=Zest(observed_indices);
xknots=Xtrn(observed_indices);
xhat_pav=xknots;
for iter1=1:maxiter
    
    % Update Z using gradient steps and hard thresholding.
    
    [Zest]=IHTZ(Zest,xknots,zknots,xhat_pav,observed_indices,...
        step_size,num_grad_steps,proj_dim,svd_method);
    
    % Update function via an LPAV fit
    zknots=Zest(observed_indices);
    
    if(strcmpi(lpav_method,'admm'))
        xhat_pav_new=lpav_admm(zknots,xknots,rho,F_mat);
    else
        % Just call MOSEK.
        xhat_pav_new=lpav(zknots,xknots);
    end
    % With this we are done with one round of learning function
    % and learning Z matrix. 
    
    % Check to see if rank needs to be increased
    frac=...
        norm(xhat_pav_new-Xtrn(observed_indices))/...
        norm(xhat_pav-Xtrn(observed_indices));
    
    reschg=abs(1-frac);
    if(reschg<10*tol_mmc && incr_rank_flag&& proj_dim<maxrank)
        % Increase rank
        proj_dim=proj_dim+incr_rank;
        proj_dim=min(proj_dim,maxrank);
        %fprintf('Increased rank to %d...\n',proj_dim);
    end
    % update lattice points. We already have correct zknots.
    xhat_pav=xhat_pav_new;
    
    % Calculate err
    relres=norm(xhat_pav-Xtrn(observed_indices))/...
        norm(Xtrn(observed_indices));
    %fprintf('iter:%d, rankest:%d, relres:%f...\n',...
        %iter1,proj_dim,relres);
    if(relres<tol_mmc)
       break;
    end
    if(decr_step_flag)
        step_size=step_size*iter1/(iter1+1);
    end
end
Xest=Interpolate(Zest,observed_indices,xhat_pav,zknots,'full');
err=Xest(val_indices)-Xval(val_indices);
rmse=norm(err)/sqrt(length(err));
end



