c=[0.1,0.5,1,2,10,20,50];
eff_rank=[6,11,16,18,19,20,20];

gca=plot(c,eff_rank,'LineWidth',5);
%set(gca,'XTickLabel',14);
ylim([1,20]);
gca1=xlabel('c','FontSize',24);
gca2=ylabel('Effective rank','FontSize',24);


%set(gca,'XTickLabel',{'0.25','0.5','0.75','0.9','0.95','0.99'})
%h_legend=legend('c=1','c=2','c=10','c=50','c=infinity');
%set(h_legend,'FontSize',40);