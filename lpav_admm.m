% An ADMM implementation of LPAV routine. 
% We shall use the scaled version of ADMM updates
% The LPAV fits the best monotonic function g that 
% approximates the relationship 
% xknots= g(zknots)


function[ret]=lpav_admm(zknots,xknots,rho,F_mat)
% We shall use y to represent xknots
[sorted_zknots,sorted_order]=sort(zknots,'ascend');
zknots=sorted_zknots;
y=xknots(sorted_order);
lip_constant=1;
b2=lip_constant*diff(zknots);
n=length(xknots);
k=2*n-2;
MAX_ITERS=2000;
u_admm_old=zeros(n+k,1);
z_admm_old=zeros(n+k,1);
%x_admm_old=zeros(n+k,1);
flag=true;
num_iters=0;
eps_abs=10^-3;
%fprintf('eps_abs set to %f..\n',eps_abs);
eps_rel=10^-3;
primal_residual=inf;
dual_residual=inf;
%%% Parameters used to change rho
%tau_incr=1;
%tau_decr=1;
%mu=10;

% We need the F matrix to update x during ADMM updates
%F_mat=Form_F_matrix(rho,n,k);
while(flag)
    % v1=u-z
    v1=u_admm_old-z_admm_old;
    %tol=tol_init/((num_iters+1)*log(num_iters+2));
    x_admm_new=Update_x_admm(n,y,v1,rho,b2,F_mat);
    
    z_admm_new=Update_z_admm(n,x_admm_new,u_admm_old);
   
    u_admm_new=Update_u_admm(u_admm_old,x_admm_new,z_admm_new);
    
    if(num_iters>0)
        dual_residual=rho*norm(z_admm_new-z_admm_old);
        primal_residual=norm(z_admm_new-x_admm_new);
    end
    
    eps_primal=sqrt(k)*eps_abs+...
        eps_rel*max(norm(x_admm_new),norm(z_admm_new));
    eps_dual=sqrt(k)*eps_abs+rho*eps_rel*norm(u_admm_new);
    if(primal_residual<eps_primal && ...
            dual_residual<eps_dual)
        flag=false;
    else
        % Update all parameters
        
        z_admm_old=z_admm_new;
        u_admm_old=u_admm_new;
    end
    %fprintf('iter:=%d, primal_residual=%f, dual-residual=%f..\n',....
     %   num_iters,primal_residual,dual_residual);
        
    %rho_new=Update_rho(primal_residual,dual_residual,...
     %   mu,rho,tau_incr,tau_decr);
    
    %F_mat=UpdateF(F_mat,rho_new,rho,n,k);
    %rho=rho_new;
    if(num_iters==MAX_ITERS)
        
        flag=false;
    end
    %fprintf('iter=%d...\n',iter);
    num_iters=num_iters+1;
    
end
%fprintf('Number of iterations=%d...\n',num_iters-1);
% We care only about the first n elements
x_admm_new=x_admm_new(1:n);
%Re-sort back the entries
ret(sorted_order)=x_admm_new;
ret=ret';
end



function[x_admm]=Update_x_admm(n,y,v1,rho,b2,F_mat)
k=2*n-2;
% Here we solve a large, sparse system of equation
% of the form Fx=g, where F is n+2k-times-n+2k
% g is n+2k
% b_bar=[0_{n-1},b2_{n-1}]
% q=-2 [y;0_k]
b_bar=[zeros(n-1,1);b2];
q=-2*[y;zeros(k,1)];
g=[-(q+rho*v1);b_bar];
x_admm=F_mat\g;
% Only the first n+k matter. The rest are dual variables associated with 
% minimizing a quadratic function over affine constraints.
x_admm=x_admm(1:n+k);
end


function[z_admm]=Update_z_admm(n,x,u)
%v2=x+u
% Clip elements after n (i.e. n+1,....,k) to 0 if negative
z_admm=x+u;
temp=z_admm(n+1:end);
temp(temp<0)=0;
z_admm(n+1:end)=temp;
end

function[u_admm]=Update_u_admm(u_admm,x_admm,z_admm)
u_admm=u_admm+x_admm-z_admm;
end


function[F]=Form_F_matrix(rho,n,k)
P_plus_rho=[(2+rho)*speye(n,n),sparse(n,k)];
mat=[sparse(k,n),rho*speye(k)];
P_plus_rho=[P_plus_rho;mat];

M=sparse(k,n);
for i=1:n-1
    M(i,i)=1;
    M(i,i+1)=-1;
end
for i=n:k
    M(i,i-n+1)=-1;
    M(i,i-n+2)=1;
end
M_bar=[M,speye(k)];

F=[[P_plus_rho,M_bar'];[M_bar,sparse(k,k)]];

end

function[rho]=Update_rho(primal_residual,dual_residual,...
        mu,rho,tau_incr,tau_decr)
    if(primal_residual>mu*dual_residual)
        rho=tau_incr*rho;
    end
    if(dual_residual>mu*primal_residual)
        rho=tau_decr*rho;
    end
end

function[F_mat]=UpdateF(F_mat,rho_new,rho,n,k)
if(abs(rho_new-rho)>eps)
     F_mat(1:n+k,1:n+k)=F_mat(1:n+k,1:n+k)+...
                (rho_new-rho)*speye(n+k);
end
end