clc;clear all;
svd_method='propack';
lpav_method='admm';
dataname='paper_reco_small';
fprintf('File:%s...\n',dataname);
[trndatafile,tstdatafile,...
    valdatafile,maxrank]=GetFileNames(dataname);
Xtrn=dlmread(trndatafile);
Xtst=dlmread(tstdatafile);
Xval=dlmread(valdatafile);
fprintf('Finished reading all data files...\n');
val_indices=find(~isnan(Xval));
tst_indices=find(~isnan(Xtst));

num_grad_steps=1; 
tol_mmc=10^-3;
maxiter=100;
step_size_vec=[9];
proj_dim=2;
incr_rank=1;
maxrank=ceil(2*maxrank/3);
incr_rank_flag=true;
decr_step_flag=false;

if(strcmpi(svd_method,'propack'))
    fprintf('Using PROPACK for SVD calculations...\n');
end
if(strcmpi(svd_method,'svds'))
    fprintf('Using SVDS for SVD calculations...\n');
end
if(strcmpi(svd_method,'randomized_svd'))
    fprintf('Using RANDOMIZED SVD for SVD calculations...\n');
end

if(strcmpi(lpav_method,'admm'))
    fprintf('Using ADMM for LPAV calculations...\n');
else
    fprintf('Using MOSEK for LPAV calculations..\n');
end

%{
Ztrue=dlmread('DATA/synthetic/lowrank_matrix.txt'); % This is the original low rank matrix
c=10;
Xtrue=1./(1+exp(-c*Ztrue));

% Now observe only a few entries
p=0.20;
fname=['DATA/xobs_c_',num2str(c),'_p_',num2str(p),'.txt'];
fprintf(fname);
fprintf('\n');
if(~exist(fname))
    fprintf('file does not exist\n');
    Xobs=HideElements(Xtrue,p);
    dlmwrite(fname,Xobs);
    fprintf('Finished creating. Writen to disk..\n');
else
    % File exists
    fprintf('File exists...\n');
    Xobs=dlmread(['DATA/synthetic/xobs_c_',num2str(c),'_p_',num2str(p),'.txt']);
end
%}

% If we are running expts on opt machine then addpath
[~,val]=unix('hostname');
if (~isempty(strfind(val,'opt')))
    % this is opt machine
    fprintf('Added path to mosek...\n');
    addpath '/data/gmravi/mosek/7/toolbox/r2013aom';
    addpath(genpath('/data/gmravi/matlab_codes/utils/PROPACK'));
else
    addpath(genpath('/Users/guestaccount/mosek/7/toolbox/r2013aom'));
    addpath(genpath('/Users/guestaccount/matlab_codes/utils/PROPACK'));
end

min_rmse=inf;

if(strcmp(lpav_method,'admm'))
    % We need an F_mat
    rho=20;
    observed_indices=~isnan(Xtrn);

    n=sum(observed_indices(:));
    
    k=2*n-2;
    P_plus_rho=[(2+rho)*speye(n),sparse(n,k)];
    mat=[sparse(k,n),rho*speye(k)];
    P_plus_rho=[P_plus_rho;mat];


    index_vec_i=zeros(k,1);
    index_vec_i(1:2:end)=[1:n-1];
    index_vec_i(2:2:end)=[1:n-1];

    index_vec_j=zeros(k,1);
    index_vec_j(1:2:end)=[1:n-1];
    index_vec_j(2:2:end)=[2:n];

    index_vec_i=[index_vec_i;n-1+index_vec_i];
    index_vec_j=[index_vec_j;index_vec_j];

    vals1=ones(k,1);
    vals1(2:2:end)=-1;

    vals2=-1*ones(k,1);
    vals2(2:2:end)=1;
    vals=[vals1;vals2];

    M=sparse(index_vec_i,index_vec_j,vals);
    M_bar=[M,speye(k)];
    F_mat=[[P_plus_rho,M_bar'];[M_bar,sparse(k,k)]];
else
    rho=inf;
    F_mat=[];
end
for j=1:length(step_size_vec)
    
    val_flag=true;
    step_size=step_size_vec(j);


    [Xest,rmse]=mmc_nofac(Xtrn,Xval,step_size,...
        proj_dim,val_indices,maxiter,num_grad_steps,maxrank,...
        incr_rank,incr_rank_flag,decr_step_flag,svd_method,...
        lpav_method,F_mat,rho,tol_mmc);

    if(rmse<min_rmse)
        min_rmse=rmse;
        Xest_best=Xest;
        opt_step_size=step_size;
        fprintf('Found better settings:err=%f..\n',rmse);
    end
    fprintf('Finished j=%d/%d,err=%f...\n',...
        j,length(step_size_vec),rmse);
end
err=Xest_best(tst_indices)-Xtst(tst_indices);
rmse=norm(err)/sqrt(length(err));
fprintf('Number of test entries=%d...\n',length(err));
fprintf('Opt step=%f,tst_err=%f\n',...
    opt_step_size,rmse);


%{
step_size_vec=10;
maxiter=500;
proj_dim=3;
incr_rank=2;
maxrank=15;
incr_rank_flag=true;
decr_step_flag=~true;
num_grad_steps=1;
tol=10^-3;
best_err=inf;
Xval=nan(size(Xobs));
[n1,n2]=size(Xobs);
val_indices=find(boolean(isnan(Xobs))& boolean((rand(n1,n2)<0.20)));
Xval(val_indices)=Xtrue(val_indices);
for j=1:length(step_size_vec)
    step_size=step_size_vec(j);
    [Xest,rmse]=mmc_nofac(Xobs,Xval,step_size,...
            proj_dim,val_indices,maxiter,num_grad_steps,...
            maxrank,incr_rank,incr_rank_flag,decr_step_flag,tol);
    %err=Xest(isnan(Xobs))-Xtrue(isnan(Xobs));
    %err_per_element_matcomp=sqrt(norm(err)^2/numel(err));
    %fprintf('RMSE=%f,step_size=%f...\n',rmse,step_size);
    if(rmse<best_err)
        best_err=rmse;
        best_step_size=step_size_vec(j);
    end
end
fprintf('RUNNING WITH OPTIMAL PARAMETERS...\n');
Xval=[];
val_indices=[];
[Xest,rmse]=mmc_nofac(Xobs,Xval,best_step_size,...
            proj_dim,val_indices,maxiter,num_grad_steps,...
            maxrank,incr_rank,incr_rank_flag,decr_step_flag,tol);
err=Xest(isnan(Xobs))-Xtrue(isnan(Xobs));
err_per_element_matcomp=sqrt(norm(err)^2/numel(err));
fprintf('RMSE=%f,Optimal_step_size=%f...\n',...
    err_per_element_matcomp,best_step_size);
%}
%{
mu=0.001;

[indices]=find(~isnan(Xobs));
observations=Xtrue(indices);
[n1,n2]=size(Xobs);

Xmatcomp=solver_sNuclearBP( {n1,n2,indices}, observations, mu);
err=Xmatcomp(isnan(Xobs))-Xtrue(isnan(Xobs));
err_per_element_matcomp=sqrt(norm(err)^2/numel(err));
fprintf('RMSE=%f...\n',err_per_element_matcomp);
fprintf(fname);
%}