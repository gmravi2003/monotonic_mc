function[ret]=projection_estimator(Xobs,p,r)
X=(1/p)*Xobs;
[U,S,V]=svds(X,r);
ret=U*S*V';
end