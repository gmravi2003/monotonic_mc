function yt = pav_interpolate_sorted(xt, x_sort,y_sort)
%IMPORTANT: I AM ASSUMING THAT x is sorted.
%[x_sort, ind_sort] = sort(x);
%y_sort = y(ind_sort);

n = length(xt);
yt = zeros(n,1);
for ii = 1:n
   
    if xt(ii)<=x_sort(1)
        yt(ii) = y_sort(1);
    elseif xt(ii)>=x_sort(end)
        yt(ii) = y_sort(end);
    else
       indl = find(x_sort<=xt(ii));
       indr = find(x_sort>=xt(ii));
       if(isempty(indl))
           display(xt(ii));
           display(x_sort(1));
       end
       xl = x_sort(indl(end));
       yl = y_sort(indl(end));
       xr = x_sort(indr(1));
       yr = y_sort(indr(1));
       if xl~=xr;
           lambda = (xt(ii)-xr)/(xl-xr);
       else
           lambda = 0;
       end
       yt(ii) = lambda*yl + (1-lambda)*yr;
    end
    
end