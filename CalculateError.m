function[err_per_element]=CalculateError(U,V,observed_indices,...
    zhat_qp,xhat_qp,Xtrue)
Zest=U*V';
[n1,n2]=size(Zest);
[Xest]=Interpolate(Zest,observed_indices, xhat_qp,zhat_qp,'full');
err_per_element=norm(Xest-Xtrue,'fro')/(n1*n2);
end