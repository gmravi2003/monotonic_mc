% Projects a matrix onto the cone of rank-r matrices
% Simply finds the best rank-r matrix
function[ret]=ProjectCone(Zest,r,svd_method)

if(strcmpi(svd_method,'svds'))
    
    [U,S,V]=svds(Zest,r);
    ret=U*(S*V');
end
if(strcmpi(svd_method,'randomized_svd'))
    
    % This uses Shiqin ma's code for randomized SVD
    % It biulds on Drineas, Mahoney and Kannan's work
    [n,m]=size(Zest);
    c=min(2*r,min(m,n));
    [U,~]=LinearTimeSVD(Zest,c,r,ones(1,m)/m);
    ret=U*(U'*Zest);
end
if(strcmpi(svd_method,'propack'))
    
    options.tol=10^-6;
    [U,S,V]=lansvd(Zest,r,'L',options);
    [n,m]=size(Zest);
    if(n<m)
        ret=(U*S)*V';
    else
        ret=U*(S*V');
    end
end
end
