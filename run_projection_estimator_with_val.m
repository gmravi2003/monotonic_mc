clc;clear all;
% If we are running expts on opt machine then addpath
[~,val]=unix('hostname');
if (~isempty(strfind(val,'opt')))
    % this is opt machine
    fprintf('Added path to mosek...\n');
    addpath '/data/gmravi/mosek/7/toolbox/r2013aom';
end

dataname='paper_reco_small';
fprintf('File:%s...\n',dataname);
if(strcmpi(dataname,'jester_small'))
    trndatafile='../lifted_mc/DATA/jester/jester-data-3.small.proc.trn.csv';
    tstdatafile='../lifted_mc/DATA/jester/jester-data-3.small.proc.tst.csv';
    valdatafile='../lifted_mc/DATA/jester/jester-data-3.small.proc.val.csv'; 
    maxrank=65;
end

if(strcmpi(dataname,'jester'))
    trndatafile='../lifted_mc/DATA/jester/jester-data-3.proc.trn.csv';
    tstdatafile='../lifted_mc/DATA/jester/jester-data-3.proc.tst.csv';
    valdatafile='../lifted_mc/DATA/jester/jester-data-3.proc.val.csv'; 
    maxrank=66;
end
if(strcmpi(dataname,'ml-100k'))
    
    % for ml-100k we shall simply use u1 data files for train & val
    % u2 files for train &test
    trndatafile='../lifted_mc/DATA/ml-100k/u1.base.proc.txt';
    valdatafile='../lifted_mc/DATA/ml-100k/u1.val.proc.txt'; 
    tstdatafile='../lifted_mc/DATA/ml-100k/u1.test.proc.txt';
    maxrank=391;
end

if(strcmpi(dataname,'paper_reco_small'))
    trndatafile='../lifted_mc/DATA/paper_reco/paper_reco_small_train.txt';
    tstdatafile='../lifted_mc/DATA/paper_reco/paper_reco_small_test.txt';
    valdatafile='../lifted_mc/DATA/paper_reco/paper_reco_small_val.txt';
    maxrank=26;
end
if(strcmpi(dataname,'paper_reco_large'))
    trndatafile='../lifted_mc/DATA/paper_reco/paper_reco_large_train.txt';
    tstdatafile='../lifted_mc/DATA/paper_reco/paper_reco_large_test.txt';
    valdatafile='../lifted_mc/DATA/paper_reco/paper_reco_large_val.txt';
    maxrank=47;
end
if(strcmpi(dataname,'barbara'))
    trndatafile='../lifted_mc/DATA/images/barbara.trn';
    tstdatafile='../lifted_mc/DATA/images/barbara.tst';
    valdatafile='../lifted_mc/DATA/images/barbara.val';
    maxrank=400;
end
if(strcmpi(dataname,'lenna'))
    trndatafile='../lifted_mc/DATA/images/lenna.trn';
    tstdatafile='../lifted_mc/DATA/images/lenna.tst';
    valdatafile='../lifted_mc/DATA/images/lenna.val';
    maxrank=406;
end
if(strcmpi(dataname,'clown'))
    trndatafile='../lifted_mc/DATA/images/clown.trn';
    tstdatafile='../lifted_mc/DATA/images/clown.tst';
    valdatafile='../lifted_mc/DATA/images/clown.val';
    maxrank=346;
end
if(strcmpi(dataname,'crowd'))
    trndatafile='../lifted_mc/DATA/images/crowd.trn';
    tstdatafile='../lifted_mc/DATA/images/crowd.tst';
    valdatafile='../lifted_mc/DATA/images/crowd.val';
    maxrank=391;
end
if(strcmpi(dataname,'girl'))
    trndatafile='../lifted_mc/DATA/images/girl.trn';
    tstdatafile='../lifted_mc/DATA/images/girl.tst';
    valdatafile='../lifted_mc/DATA/images/girl.val';
    maxrank=405;
end

if(strcmpi(dataname,'cameraman'))
    trndatafile='../lifted_mc/DATA/images/cameraman.trn';
    tstdatafile='../lifted_mc/DATA/images/cameraman.tst';
    valdatafile='../lifted_mc/DATA/images/cameraman.val';
    maxrank=393;
end
Xtrn=dlmread(trndatafile);
Xtst=dlmread(tstdatafile);
Xval=dlmread(valdatafile);

trn_indices=find(~isnan(Xtrn));
val_indices=find(~isnan(Xval));
tst_indices=find(~isnan(Xtst));

proj_dim_vec=[ceil(maxrank/4),ceil(maxrank/3),ceil(maxrank/2),...
    ceil(2*maxrank/3),ceil(0.75*maxrank)];

proj_dim_vec=[12];
Xobs=Xtrn;
Xobs(isnan(Xobs))=0;
min_rmse=inf;
proj_dim_max=max(proj_dim_vec);
[U,S,V]=svds(Xobs,proj_dim_max);
for j=1:length(proj_dim_vec)
    proj_dim=proj_dim_vec(j);
    
    Zest=U(:,1:proj_dim)*S(1:proj_dim,1:proj_dim)*V(:,1:proj_dim)';

    % learn a g function by calling Lipschitz PAV
    % X=g(Z)

    zknots=Zest(trn_indices);
    xknots=Xobs(trn_indices);
    xhat_pav = lpav(zknots,xknots);
    Xest=Interpolate(Zest,trn_indices,xhat_pav,zknots,'full');
        
    err=Xval(val_indices)-Xest(val_indices);
    rmse=sqrt(norm(err)^2/length(err));
    if(rmse<min_rmse)
        min_rmse=rmse;
        Xest_best=Xest;
        opt_proj_dim=proj_dim;
        fprintf('Found better settings:err=%f..\n',rmse);
    end
end
fprintf('proj_dim=%f..\n',opt_proj_dim);
err=Xest_best(tst_indices)-Xtst(tst_indices);
rmse=sqrt(norm(err)^2/length(err));
fprintf('Number of test entries=%d...\n',length(err));
fprintf('Opt_proj_dim=%f,tst_err=%f\n',...
    opt_proj_dim,rmse);